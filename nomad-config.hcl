job "example-app" {
  datacenters = ["dc1"]

  group "app-group" {
    count = 3

    task "app-task" {
      driver = "docker"

      config {
        image = "dockerhub-username-placeholder/docker_image_placeholder"
        port_map = {
          web = 8080 + count.index
        }
      }

      resources {
        cpu    = 100
        memory = 128
      }
    }
  }
}
