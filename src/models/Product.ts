export type TProduct = {
  id: string;
  name: string;
  price: number;
  description: string;
  categories: string[];
  photo: string;
}

export type TBasketItem = {
  product: TProduct,
  quantity: number;
}

export type TBasket = {
  items: TBasketItem[];
  isRead: boolean;
  isOpen: boolean,
}