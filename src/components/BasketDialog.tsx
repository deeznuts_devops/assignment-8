import React from 'react'
import Image from 'next/image'
import { Button } from './ui/button'
import { useAppDispatch, useAppSelector } from '@/hooks/useRedux'
import { basketActions } from '@/store/basket/slice'
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog"
import { QuantityGroup } from './QuantityGroup'

export const BasketDialog: React.FC = () => {
  const basket = useAppSelector(state => state.basket);
  const dispatch = useAppDispatch();
  return (
    <Dialog open={basket.isOpen} onOpenChange={() => dispatch(basketActions.openChange())}>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>
            {basket.items.length > 0 && (
              "Корзина"
            )}
          </DialogTitle>
          <DialogDescription>
          {basket.items.length > 0 ? (
            <>
              {basket.items.map((item) => (
                <div className="flex items-center justify-between gap-2" key={item.product.id}>
                  <div className="flex gap-2">
                    <div className="h-16 w-16 rounded-md relative overflow-hidden">
                      <Image objectFit="cover" fill src={item.product.photo} alt={item.product.name} />
                    </div>
                    <div className="text-foreground line-clamp-1 max-w-[50%]">
                      <div className="text-xl font-bold">{item.product.name}</div>
                      <div>
                        <span className="text-lg">${item.quantity * item.product.price}</span>
                        {item.quantity > 1 && (
                          <span className="text-sm opacity-80"> / ${item.product.price}</span>
                        )}
                      </div>
                    </div>
                  </div>
                  <QuantityGroup item={item} />
                </div>
              ))}
            </>
          ) : (
              <div className="flex flex-col flex-center relative">
                <div className="w-2/3 aspect-square relative overflow-hidden">
                  <Image fill objectFit="contain" src="/emptyBasket.svg" alt="Корзина пуста" />
                </div>
                <div className="font-bold text-xl text-foreground mb-2">Ваша корзина пуста</div>
                <div className="text-center mb-4">Добавьте товары с каталога <br /> и они появятся в корзине</div>
                <Button className="mx-auto" onClick={() => dispatch(basketActions.openChange())}>
                  Перейти в каталог
                </Button>
              </div>
            )}
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  )
}
