import React from 'react'
import { basketActions } from '@/store/basket/slice'
import { ShoppingBasketIcon } from 'lucide-react'
import { Button } from './ui/button'
import { Badge } from './ui/badge'
import { useAppDispatch, useAppSelector } from '@/hooks/useRedux'

export const ShoppingCartButton = () => {
  const basket = useAppSelector(state => state.basket);
  const dispatch = useAppDispatch();
  return (
    <div className="fixed right-3 bottom-3 z-[3]">
        <Button variant="ghost" className="border shadow-xl h-12" onClick={() => dispatch(basketActions.openChange())}>
          <ShoppingBasketIcon />
        </Button>
        {basket.items.length > 0 && (
          <Badge variant={basket.isRead ? "default" : "destructive"} className={`absolute -top-3 -left-3 rounded-full`}>{basket.items.length}</Badge>
        )}
      </div>
  )
}
